var express = require('express');
var app = express();
var mdAuth = require('../middlewares/authentication');

var Streaming = require('../models/streaming');

// Rutas
// app.get('/', (req, res, next) => {
//     res.status(200).json({
//         ok: true,
//         message: 'Succesful Request'
//     });
// })


// =================
// Get all streamings
// =================
// app.get('/', mdAuth.verifyToken, (req, res, next) => {

//     const { status } = req.query;
//     const queryObj = {};

//     if (status) {
//       queryObj['status'] = status;
//     }

//     // console.log('::queryObj:::', queryObj);

//     Streaming.find(queryObj, 'room_id user_id created_at finished_at status')
//         .populate('user_id', 'name last_name email')
//         .populate('room_id', 'name slug status')
//         .exec((err, models) => {
//             // Catch error
//             if (err) {
//                 return res.status(500).json({
//                     ok: false,
//                     message: 'Error in model collection',
//                     errors: err
//                 });
//             }
//             // Successful response
//             res.status(200).json({
//                 ok: true,
//                 data: models,
//                 user: req.user
//             });
//         });
// })

module.exports = app;