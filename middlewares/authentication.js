var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED;

// =================
// Validate Token
// =================
exports.verifyToken = function(req, res, next){
    // Get token from header Authorization
    var token = req.headers.authorization;
    
    // Verify
    jwt.verify( token, SEED, (err, decode) => {
        // Catch error
        if (err) {
            return res.status(401).json({
                ok: false,
                message: 'Invalid Token',
                errors: err
            });
        }

        //Pass Validad Decoded User Data to Request
        req.user = decode.user;

        // All OK continue!
        next();
    });
};

    