var moongose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = moongose.Schema;

var streamingSchema = new Schema({
    room_id: { type: Schema.Types.ObjectId, ref:'Room', required: [true, 'La sala es obligatoria'] },
    user_id: { type: Schema.Types.ObjectId, ref:'User', required: [true, 'El dueño de la sala es obligatorio'] },
    status: { type: Boolean, Boolean: true, required: [true, 'El estado es obligatorio'], default: 1},
    created_at: { type : Date, default: Date.now },
    finished_at: { type: Date }
});

streamingSchema.plugin( uniqueValidator, {message: '{PATH} debe ser único'})

module.exports = moongose.model('Streaming', streamingSchema);