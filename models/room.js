var moongose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = moongose.Schema;

var roomSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref:'User', required: [true, 'El usuario es obligatorio'] },
    name: { type: String, required: [true, 'El nombre de sala es obligatorio'] },
    status: { type: Boolean, Boolean: true, required: [true, 'El estado es obligatorio'], default: 1},
    slug: { type: String, required: [true, 'El slug es obligatorio'] }
});

roomSchema.plugin( uniqueValidator, {message: '{PATH} debe ser único'})

module.exports = moongose.model('Room', roomSchema);