var Streaming = require('../models/streaming');

// Get active streamings
async function getActiveStreamings() {

    const queryObj = {};
    queryObj['status'] = true;

    return await Streaming.find(queryObj, 'room_id user_id created_at finished_at status')
        // .populate('user_id', 'name last_name email')
        // .populate('room_id', 'name slug status')
        .exec((err, models) => {
            console.log('\x1b[32m%s\x1b[0m', 'Request');
            // Catch error
            if (err) {
                console.log(err);
            }

            // Successful response
            // console.log(models);
            return models;
        });    
}


module.exports = {
    getActiveStreamings
};