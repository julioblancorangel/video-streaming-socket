// Requires
var express = require('express');
var moongose = require('mongoose');
var bodyParser = require('body-parser');
var PORT = require('./config/config').PORT;

// Inicializar variables
var app = express();
// Escuchar peticiones
const server = app.listen(PORT, () => {
    console.log('Video Streaming Socket corriendo en el puerto 3080: \x1b[32m%s\x1b[0m', 'online');
})
var Streaming = require('./models/streaming');
var User = require('./models/user');
var Room = require('./models/room');

const formatMessage = require('./utils/messages');
const {
	userJoin,
	getCurrentUser,
	userLeave,
	getRoomUsers
} = require('./utils/users');

//websocket
const SocketIO = require('socket.io');
const io = SocketIO(server);
const botName = 'Xtasisex Bot';

// Enable CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');

    // authorized headers for preflight requests
    // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
    res.header('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept');
    next();

    app.options('*', (req, res) => {
        // allowed XHR methods  
        res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
        res.send();
    });
});

// Body Parser Configure
// Parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false}));
// Parse application/json
app.use(bodyParser.json());


// ==============================================================================================
// CONNECT TO DATABASE
moongose.connection.openUri('mongodb://localhost:27017/video-streaming', (err, res) => {
    if(err) throw err;
    console.log('Base de Datos: \x1b[32m%s\x1b[0m', 'conectada');
})
// ==============================================================================================

const activeRooms = [ 
	{id: 1 , slug: 'kitty-69', name: 'Kitty' },
	{id: 2 , slug: 'hello-friendly', name: 'Friendly Hamster' },
	{id: 3 , slug: 'olo-70', name: 'Olo' },
];

// ==============================================================================================
// GET ACTIVE STREAMINGS
// getActiveStreamings()
//     .then(response => {
// 		const streamingArray = response;
// 		// console.log('LIST OF ACTIVE STREAMINGS');
// 		console.log('\x1b[32m%s\x1b[0m', 'LIST OF ACTIVE STREAMINGS');
// 		// console.log(response);
// 		streamingArray.forEach(element => {
// 			console.log(element);
// 		});
//     })
//     .catch(err => {
// 		console.log(err);
//         // Deal with the fact the chain failed
//     });
// ==============================================================================================

io.on('connection', (socket) => {
	console.log('New connection to socket: ', socket.id);

	// ROOM
	socket.on('joinRoom', ({ username, room }) => {
		const user = userJoin(socket.id, username, room);
		console.log('User Joined to Room');
		console.log(user);
		socket.join(user.room);

		// Welcome current user
		socket.emit('chat:message', formatMessage(botName, 'Welcome to this Room!'));

		// Broadcast when a user connects
		// socket.broadcast
		// 	.to(user.room)
		// 	.emit(
		// 		'message',
		// 		formatMessage(botName, `${user.username} has joined the chat`)
		// 	);

		// Send users and room info
		io.to(user.room).emit('roomUsers', {
			room: user.room,
			users: getRoomUsers(user.room)
		});
	});
	// ROOM

	// LISTEN FOR MESSAGE
	socket.on('chat:message', (data) => {
		// const user = getCurrentUser(socket.id);
		io.to(user.room).emit('chat:message', formatMessage(data.username, data.message));
		// console.log(data);
		// io.emit('chat:message', data)
		//io.sockets.emit('chat:message', data);
	});
	// LISTEN FOR MESSAGE

	// LISTEN: GET ACTIVE ROOMS
	socket.on('get:active_rooms', (data) => {
		console.log('get:active_rooms requested');
		socket.emit('get:active_rooms', activeRooms);
	});
	// LISTEN: GET ACTIVE ROOMS
	
	// LISTEN: ACTIVATE ROOM
	socket.on('set:active_room', (data) => {
		console.log('set:active_room requested');
		socket.emit('set:active_room', activeRooms);
	});
	// LISTEN: ACTIVATE ROOM

	// socket.on('chat:typing', (data)=>{
	// 	//console.log(data);
	// 	socket.broadcast.emit('chat:typing', data);
	// });


	//nuevo video streaming, recibir y emitir imagenes para streaming
	// socket.on('stream', (image)=>{
	// socket.broadcast.emit('play streaming', image);
	// console.log('en play del streaming en servidor');
	// });

});


async function getActiveStreamings() {
    
	const queryObj = {};
	queryObj['status'] = true;

	return await Streaming.find(queryObj, 'room_id user_id created_at finished_at status')
		.populate('user_id', 'name last_name email')
		.populate('room_id', 'name slug status')
		.exec();  
}